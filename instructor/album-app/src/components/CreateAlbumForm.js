import React, { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { addAlbum } from "../actions";

const CreateAlbumForm = ({ fetchAlbum, setFetchAlbum }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [title, setTitle] = useState("");
	const [artist, setArtist] = useState("");
	const [price, setPrice] = useState("");
	const [tracks, setTracks] = useState("");

	const handleSubmit = async (e) => {
		e.preventDefault();
		// console.log("submit form is clicked");
		// console.log(title);
		// console.log(artist);
		// console.log(price);
		// console.log(tracks);

		// dispatch addAlbum action
		dispatch(
			addAlbum({
				title: title,
				artist: artist,
				price: price,
				tracks: tracks,
			})
		)
			.then(() => {
				console.log("addAlbum is successful");
				setFetchAlbum(!fetchAlbum);
			})
			.catch(() => {})
			.finally(() => {
				console.log("addAlbum thunk function is completed");
				history.push("/");
			});

		//this addAlbum takes 5secs to finish
		// await addAlbum({
		// 	title: title,
		// 	artist: artist,
		// 	price: price,
		// 	tracks: tracks,
		// });

		// console.log("addAlbum thunk function is completed");
		// history.push("/");
	};

	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			<h3>Add New Album</h3>
			<form onSubmit={handleSubmit} autoComplete="off">
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="title">Title:</label>
						<input
							id="title"
							type="text"
							className="form-control"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="artist">Artist:</label>
						<input
							id="artist"
							type="text"
							className="form-control"
							value={artist}
							onChange={(e) => setArtist(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="price">Price:</label>
						<input
							id="price"
							type="number"
							className="form-control"
							value={price}
							onChange={(e) => setPrice(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="tracks">Number of tracks:</label>
						<input
							id="tracks"
							type="number"
							className="form-control"
							value={tracks}
							onChange={(e) => setTracks(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<input
							type="submit"
							value="Create Album"
							className="btn btn-outline-secondary"
						/>
					</div>
				</div>
			</form>
		</div>
	);
};

// refactor below line to useDispatch
//export default connect(null, { addAlbum })(CreateAlbumForm);
export default CreateAlbumForm;
