import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

// const header = React.createElement("h1", null, "My header");
// const text = React.createElement("p", {className:"hello:text", id: "hello-id"}, "Hello world!!!");
// const main = React.createElement("div", {className: "hello-container"}, [header, text]);

// This is using React.createElement
// const main = React.createElement( "div", { id: "ice-cream" }, [
//   React.createElement( "h1", null, "Ice Cream Flavours" ),
//   React.createElement( "ul", { class: "ice-cream-list" }, [
//   React.createElement( "li", null, "Vanilla" ),
//   React.createElement( "li", null, "Chocolate" ),
//   React.createElement( "li", null, "Raspberry Ripple" )
//   ] )
//   ] );
function getFlavors() {
  return (
    <React.Fragment>
      <h2>Ice cream flavors</h2>
      <ul className ="border">
        <li className = "raspberry_color">Vanilla</li>
        <li style={{color:"red"}}>Chocolate</li>
        <li style={{color: "blue"}}>Raspberry Ripple</li>
      </ul>
    </React.Fragment>
  );
}
const shopName = "Alltsate Ice creams";
// This is using javascript xml
const main = (
  <section>
    <div>
      <h1>{shopName}</h1>
    </div>
    <div id="ice-cream">
      {getFlavors()}
    </div>
  </section>
);


ReactDOM.render(main, document.getElementById("root"));


