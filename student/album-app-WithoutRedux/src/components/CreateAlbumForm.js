import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import axios from "axios";

const CreateAlbumForm = ({ refresh, setRefresh }) => {
	const history = useHistory();
	const [title, setTitle] = useState("");
	const [artist, setArtist] = useState("");
	const [price, setPrice] = useState("");
	const [tracks, setTracks] = useState("");

	const handleSubmit = (e) => {
		e.preventDefault();
		console.log("submit form is clicked");
		console.log(title);
		console.log(artist);
		console.log(price);
		console.log(tracks);

		axios
			.post("http://localhost:8088/albums", {
				title: title,
				artist: artist,
				price: price,
				tracks: tracks,
			})
			.then(() => {
				console.log("album created!");
				setRefresh(!refresh); // lifting state up to parent
				history.push("/");
			});
	};

	return (
		<div className="container">
			<h3>Add New Album</h3>
			<form onSubmit={handleSubmit}>
				<div className="form-row">
					<div className="form-group col-md-5">
						<label>Title:</label>
						<input
							id="title"
							type="text"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label>Artist:</label>
						<input
							id="artist"
							type="text"
							value={artist}
							onChange={(e) => setArtist(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<label>Price:</label>
						<input
							id="price"
							type="number"
							value={price}
							onChange={(e) => setPrice(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label>Number of tracks:</label>
						<input
							id="tracks"
							type="number"
							value={tracks}
							onChange={(e) => setTracks(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-group">
					<input type="submit" value="Create Album" />
				</div>
			</form>
		</div>
	);
};

export default CreateAlbumForm;
