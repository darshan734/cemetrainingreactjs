import React from "react";

const AlbumArtist = (props) => (
	<h6 className="album-artist card-subtitle mb-2 text-muted">{props.artist}</h6>
);

export default AlbumArtist;
