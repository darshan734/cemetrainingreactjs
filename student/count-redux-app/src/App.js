import React from "react";
import { connect } from "react-redux";

const App = (props) => (
	//console.log("App props is ", props);
	<React.Fragment>
		<div>App component</div>
		<p>Count is {props.count}</p>
		<p>Weight is {props.weight}</p>
	</React.Fragment>
);

// mapStateToProps receives the redux store/state
const mapStateToProps = (state) => {
	console.log("state is ", state);
	return {
		count: state.count,
		weight: state.weight,
	};
};

export default connect(mapStateToProps)(App);
