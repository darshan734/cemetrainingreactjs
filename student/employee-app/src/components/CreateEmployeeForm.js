import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addEmployee } from "../actions";

const CreateEmployeeForm = ({ fetchEmployee, setFetchEmployee }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");
  const [salary, setSalary] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    // dispatch addEmployee action
    dispatch(
      addEmployee({
        id: id,
        name: name,
        age: age,
        email: email,
        salary: salary,
      })
    )
      .then(() => {
        console.log("addEmployee is successful");
        setFetchEmployee(!fetchEmployee);
      })
      .catch(() => {})
      .finally(() => {
        console.log("addEmployee thunk function is completed");
        history.push("/");
      });
  };

  return (
    <div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
      <h3>Add New Employee</h3>
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="id">ID:</label>
            <input
              id="id"
              type="number"
              className="form-control"
              value={id}
              onChange={(e) => setId(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="name">Name:</label>
            <input
              id="name"
              type="text"
              className="form-control"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label htmlFor="age">Age:</label>
            <input
              id="age"
              type="number"
              className="form-control"
              value={age}
              onChange={(e) => setAge(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="email">Email:</label>
            <input
              id="email"
              type="text"
              className="form-control"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label htmlFor="salary">Salary:</label>
            <input
              id="salary"
              type="number"
              className="form-control"
              value={salary}
              onChange={(e) => setSalary(e.target.value)}
            />
          </div>
        </div>
        <div className="form-group">
          <input type="submit" value="Create Employee" />
        </div>
      </form>
    </div>
  );
};

export default CreateEmployeeForm;
