import React from "react";

const EmployeeName = ({ name }) => (
  <div>
    {/* <h3 className="album-artist card-subtitle mb-2 text-muted">
      {props.name.id}
    </h3> */}
    <h3 className="album-artist card-subtitle mb-2 text-muted">{name}</h3>
  </div>
);

export default EmployeeName;
