import React from "react";

const Banner = () => (
  <section className="jumbotron text-center">
    <div className="container">
      <h1 className="jumbotron-heading">Employee example</h1>
      <p className="lead text-muted">
        Something short and leading about the collection below—its contents, the
        creator, etc. Make it short and sweet, but not too short so folks don't
        simply skip over it entirely.
      </p>
      <p>
        <a
          href="https://bitbucket.org/neohenry/cemetrainingreactjs/src/master/instructor/album-app/"
          className="btn btn-secondary my-2"
          target="_blank"
          rel="noreferrer"
        >
          Get the source code
        </a>
      </p>
    </div>
  </section>
);

export default Banner;
