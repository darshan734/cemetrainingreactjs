import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Banner from "./Banner";
import Navbar from "./Navbar";
import EmployeesListing from "./EmployeesListing";
import CreateEmployeeForm from "./CreateEmployeeForm";
import { fetchEmployees } from "../actions"; // pick up index.js by default

const App = () => {
  const dispatch = useDispatch();
  const [fetchEmployee, setFetchEmployee] = useState(false);

  useEffect(() => {
    dispatch(fetchEmployees()); // dispatch fetchEmployees action
  }, [fetchEmployee, fetchEmployees]); // run this at start and whenever fetchEmployee is changed

  return (
    <Router>
      <div className="app">
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Banner />
            <EmployeesListing />
          </Route>
          <Route path="/add">
            <CreateEmployeeForm
              fetchEmployee={fetchEmployee}
              setFetchEmployee={setFetchEmployee}
            />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
