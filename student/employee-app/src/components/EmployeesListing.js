import React, { useState } from "react";
import Employee from "./Employee";
import { useSelector } from "react-redux";

const EmployeesListing = () => {
  const employees = useSelector((state) => state.employees.entities);
  const error = useSelector((state) => state.employees.error);
  const loading = useSelector((state) => state.employees.loading);

  if (error) {
    return <div className="d-flex justify-content-center">{error.message}</div>;
  }

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  return (
    <div className="container">
      <div className="row">
        {employees.map((employee) => (
          <Employee
            name={employee.name}
            id={employee.id}
            age={employee.age}
            email={employee.email}
            salary={employee.salary}
          />
        ))}
      </div>
    </div>
  );
};

export default EmployeesListing;
