import React from "react";
import ShowHideButton from "./ShowHideButton";

const EmployeeDetails = ({ id, age, email, salary, visible }) => {
  return (
    visible && (
      <ol className="album-tracks">
        {/* {props.employee.map((employee) => ( */}
        <li key={id}>ID: {id}</li>
        <li key={age}>Age: {age}</li>

        <li key={email}>Email: {email}</li>
        <li key={salary}>Salary: {salary}</li>
        {/* ))} */}
      </ol>
    )
  );
};

export default EmployeeDetails;
