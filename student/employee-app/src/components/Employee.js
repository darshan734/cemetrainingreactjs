import React, { useState } from "react";
import EmployeeName from "./EmployeeName";
import EmployeeDetails from "./EmployeeDetails";
import ShowHideButton from "./ShowHideButton";

const Employee = ({ name, id, age, email, salary }) => {
  const [visible, setVisibity] = useState(true);

  return (
    <div className="col-md-4">
      <div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
        <div className="card-body">
          <EmployeeName name={name} />
          <EmployeeDetails
            id={id}
            age={age}
            email={email}
            salary={salary}
            visible={visible}
          />
          <ShowHideButton toggle={setVisibity} visible={visible} />
        </div>
      </div>
    </div>
  );
};

export default Employee;
