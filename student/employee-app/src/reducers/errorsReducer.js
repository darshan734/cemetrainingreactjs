const errorsReducer = (state = [], action) => {
  console.log(`received ${action.type} dispatch in errorsReducer`);
  switch (action.type) {
    case "FETCH_EMPLOYEES_FAILURE":
    case "ADD_EMPLOYEE_FAILURE":
      return [...state, action.payload];
    default:
      return state;
  }
};

export default errorsReducer;
