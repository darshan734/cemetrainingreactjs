import { combineReducers } from "redux";
import employeesReducer from "./employeesReducer";
import errorsReducer from "./errorsReducer";

const rootReducer = combineReducers({
  employees: employeesReducer,
  errors: errorsReducer,
});

export default rootReducer;
