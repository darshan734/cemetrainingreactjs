import axios from "axios";

// for our Action Creators
export const fetchEmployeesBegin = () => {
  return {
    type: "FETCH_EMPLOYEES_BEGIN",
  };
};

export const fetchEmployeesSuccess = (employees) => {
  return {
    type: "FETCH_EMPLOYEES_SUCCESS",
    payload: employees,
  };
};

export const fetchEmployeesFailure = (err) => {
  return {
    type: "FETCH_EMPLOYEES_FAILURE",
    payload: { message: "Failed to fetch employees.. please try again later" },
  };
};

// to be call by the components
export const fetchEmployees = () => {
  // returns the thunk function
  return (dispatch, getState) => {
    dispatch(fetchEmployeesBegin());
    console.log("state after fetchEmployeesBegin", getState());
    axios.get("http://localhost:8080/api/all").then(
      (res) => {
        //console.log(res);
        //setAlbums(res.data); // TODO dispatch FETCH_EMPLOYEES_SUCCESS
        setTimeout(() => {
          dispatch(fetchEmployeesSuccess(res.data));
          console.log("state after fetchEmployeesSuccess", getState());
        }, 3000);
      },
      (err) => {
        // dispatch FETCH_EMPLOYEES_FAILURE
        dispatch(fetchEmployeesFailure(err));
        console.log("state after fetchEmployeesFailure", getState());
      }
    );
  };
};

export const addEmployeeBegin = () => {
  return {
    type: "ADD_EMPLOYEE_BEGIN",
  };
};

export const addEmployeeSuccess = () => {
  return {
    type: "ADD_EMPLOYEE_SUCCESS",
  };
};

export const addEmployeeFailure = (err) => {
  return {
    type: "ADD_EMPLOYEE_FAILURE",
    payload: { message: "Failed to add new employee.. please try again later" },
  };
};

function delay(t, v) {
  return new Promise(function (resolve) {
    setTimeout(resolve.bind(null, v), t);
  });
}

export const addEmployee = (employee) => {
  // returns our async thunk function
  return (dispatch, getState) => {
    return axios.post("http://localhost:8080/api/save", employee).then(
      () => {
        console.log("employee created!");
        // this is where we can dispatch ADD_EMPLOYEE_SUCCESS
        dispatch(addEmployeeSuccess());
      },
      (err) => {
        dispatch(addEmployeeFailure(err));
        console.log("state after addEmployeeFailure", getState());
      }
    );
  };
};
