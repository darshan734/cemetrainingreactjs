import React from "react";

// Class components
class Greet extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <h1>
        Class component - Welcome, {this.props.person} to {this.props.name}!
      </h1>
    );
  }
}

// Arrow functions
// const Greet = (props) => (
//   <h1>
//     Hello, {props.person} from {props.name}!
//   </h1>
// );

export default Greet;
