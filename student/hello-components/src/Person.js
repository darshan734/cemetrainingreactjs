import React from "react";
import ReactDOM from "react-dom";
import Greet from "./Greet";

class Person extends React.Component {
  constructor(props) {
    super(props);
    // set initial state
    this.state = { height: 185, weight: 200 };
    // this.grow = this.grow.bind(this);
  }

  // grow() {
  //   this.setState({ height: this.state.height + 1 });
  //   console.log(this.state.height);
  // }

  // Using arrow function
  growHeight = () => {
    this.setState(function (state) {
      return { height: this.state.height + 1 };
    });

    // this.setState((state) => ({ height: this.state.height + 1 }));
    console.log(this.state.height);
  };

  growWeight = () => {
    this.setState(function (state) {
      return { weight: this.state.weight + 1 };
    });
  };

  render() {
    return (
      <div>
        <h3>
          <Greet person={this.props.name} name="Allstate" />
        </h3>
        <p>
          {" "}
          {this.props.name} weighs {this.state.weight} lbs and is{" "}
          {this.state.height} cm tall
        </p>
        <button onClick={this.growHeight}>Grow Height</button>
        <button onClick={this.growWeight}>Grow Weight</button>
      </div>
    );
  }
}

export default Person;
